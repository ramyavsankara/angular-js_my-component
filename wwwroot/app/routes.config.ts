import { Home } from './home/Home';
import { About } from './about/About';
import { People } from './people/People';
import { PersonDetail } from './people/PersonDetail';
import { Song } from './song/Song';
import { Route, Router } from 'angular2/router';

export var Routes = {
  home: new Route({path: '/', as: 'Home', component: Home}),
  about: new Route({path: '/about', as: 'About', component: About}),
  people: new Route({ path: '/people', as: 'People', component: People }),
  detail: new Route({ path: '/people/:id', as: 'Detail', component: PersonDetail }),
  //song: new Route({path: '/song', as: 'Song', component: Song })

  // 404 routing, or otherwise routing logic
// if route is not found go Home
//routes.MapRoute("spa-fallback", "{*anything}",  { controller = "Home", action = "Index" });
};

export const APP_ROUTES = Object.keys(Routes).map(r => Routes[r]);
